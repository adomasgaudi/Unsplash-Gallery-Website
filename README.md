# Unsplash gallery website

This website connects to the Unsplash API to generate images based on the query of the client. 
You can visit the website at:
* [unsplash-gallery-2](https://unsplash-gallery-2.web.app/)
* [unsplash-gallery-2](https://unsplash-gallery-2.firebaseapp.com/)
## Tools

- The website is built using JavaScript library [react](https://reactjs.org/)
- The website is Hosted on the [Firebase](https://firebase.google.com/) platform
### Dependecies used: ###
  * [Axios](https://axios-http.com/) - to connect with the unsplash API
  * [Matrial UI](https://mui.com/) - React User Interface library 
  * [Dotenv](https://github.com/motdotla/dotenv) - create environmental variables from .env file
  
## How to use
- Step 1. Open one of the links
- Step 2. Type in the seachbar what photos you wan't to find. 
- Step 3. Enjoy your selection



