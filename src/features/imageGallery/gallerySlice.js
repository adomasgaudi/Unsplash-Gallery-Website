import { createSlice } from "@reduxjs/toolkit";



const initialInputState = {
  // query vars
  queryValue: 'hire',
  queryLog: [{value: "me", count: 1}, {value: "media", count: 1}, {value: "park", count: 1}],
  suggestedQuery: [{value: "media", count: 1}, {value: "park", count: 1}, {value: "hire", count: 1}],

  //
  photosArray: [''],
  loading: {
    finished: true,
    found: true
  },
  homeView: true
  
}

const inputSlice = createSlice({
  name: "input",
  initialState: initialInputState,
  reducers:
    {
      checkQuery: (state, action) => {
        state.queryValue = action.payload
        // activates useEffect in imageGallery
      },
      uploadToGallery: (state, action) => {
        state.photosArray = [...action.payload]
      },
      isLoading: (state, action) => {
        state.loading = { 
          finished: action.payload.finished, 
          found: action.payload.found
        }
      },
      toQueryLog: (state, action) =>{

        // if value exists
        const check_existing = [...state.queryLog].filter(item => item.value === action.payload)
        if(check_existing.length != 0){

          // find index and add count
          const objIndex = state.queryLog.findIndex((obj => obj.value == action.payload));
          state.queryLog[objIndex].count++;

          // order by count and add to the suggested queries
          const orderedByCount = [...state.queryLog].sort((a, b) => (a.count > b.count) ? -1 : 1)
          state.suggestedQuery = [...orderedByCount]
        }

        // if value doesn't yet exist
        else {
          // add to front of array with count: 1
          state.queryLog.unshift({value: action.payload, count: 1})

          // order by count and add to suggested queries
          const orderedByCount = [...state.queryLog].sort((a, b) => (a.count > b.count) ? -1 : 1)
          state.suggestedQuery = [...orderedByCount]

        }
      },
      deleteQuery: (state, action) => {

        // spread in contents of filtered array 
        state.queryLog = [ 
          ...state.queryLog.filter(item => 
            item.value !== action.payload)
        ]
        state.suggestedQuery = [ 
          ...state.suggestedQuery.filter(item => 
            item.value !== action.payload)
        ]

      },
      changeView: (state, action) => {
        state.homeView = action.payload
      }
    }
})


export const { 
  checkQuery, uploadToGallery, isLoading , toQueryLog, deleteQuery, changeView
} = inputSlice.actions

export default inputSlice