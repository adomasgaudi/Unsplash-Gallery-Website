import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import axios from "axios";
import { isLoading, toQueryLog, uploadToGallery } from "./gallerySlice";
import { CircularProgress } from '@mui/material';

export default function ImageGallery() {
  const dispatch = useDispatch();
  const queryValue = useSelector((state) => state.gallery.queryValue);
  const queryLog = useSelector((state) => state.gallery.queryLog);
  const photosArray = useSelector((state) => state.gallery.photosArray);
  const loading = useSelector((state) => state.gallery.loading);

  const ACCESS_KEY = "dvWWWCNc8IsT4kpnqYdoB7FfB-bHdHiqOvk1prjpgEI";
  const unsplash_url = `https://api.unsplash.com/search/photos?page=1&query=${queryValue}&client_id=${ACCESS_KEY}&per_page=20`;



  const fetchData = async (url) => {
    dispatch(isLoading({ finished: false, found: true }));

    const res = await axios.get(url);
    const data_test = await res.data;
    // console.log("res", { data_test });


    if (data_test.total === 0) dispatch(isLoading({ finished: true, found: false }));
    

    // if query is valid, then add to saved queries
    else {
      console.log("data uploaded");
      dispatch(uploadToGallery([...data_test.results]));
      dispatch(toQueryLog(queryValue))

      dispatch(isLoading({ finished: true, found: true }));
    }
  };


  useEffect(() => {
    fetchData(unsplash_url);
  }, [queryValue]);


  return (
    <div className="imageGallery">

      {!loading.finished && <div className="center">
          <CircularProgress/>
      </div>}

      {!loading.found && <div className="center">
        <h1 ><strong>Not Found</strong></h1>
      </div>
      }
      {photosArray[0].urls && (
        <ul>
          {photosArray.map((photo) => (
            <li key={photo.id}>
              <img src={photo.urls.regular} alt={photo.alt_description} />
            </li>
          ))}
        </ul>
      )}
    </div>
  );
}
