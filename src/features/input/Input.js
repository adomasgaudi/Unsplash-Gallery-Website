import React, { useRef } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { checkQuery, deleteQuery } from "../imageGallery/gallerySlice";

export default function Input() {
  const input_ref = useRef();
  const dispatch = useDispatch();
  const queryLog = useSelector((state) => state.gallery.queryLog);
  const suggestedQuery = useSelector((state) => state.gallery.suggestedQuery);
  const homeView = useSelector(state => state.gallery.homeView)

  const submitHandler = (e) => {
    e.preventDefault();

    //change query
    dispatch(checkQuery(input_ref.current.value));
    // queryLog will be changed in imageGallery component
  };

  const spanClickHandler = (e) => {
    input_ref.current.value = e.target.innerHTML;
    dispatch(checkQuery(e.target.innerHTML));
  };

  const suggestedClickHandler = (e) => {
    input_ref.current.value = e.target.innerHTML;
    dispatch(checkQuery(e.target.innerHTML));
  };

  const closeHandler = (e) => {
    const input_val = e.target.parentElement.querySelector("#query_value_span").innerHTML;
    dispatch(deleteQuery(input_val));
  };

  let search = {display: "none"}
  if(homeView) {
    search = { display: "none" };
  } else {
    search = { display: "block"}
  }

  return (
    < >
      <form className="navbar" action="" onSubmit={(e)=>submitHandler(e)}>
        <div className="inputBar">
          <input style={search} type="text" ref={input_ref} /> 
          <button type="submit">SS</button>
        </div>
        {homeView && suggestedQuery && (
          <ul className="homeView">
            {suggestedQuery.map((query) => (
              <li
                id="suggested_span"
                onClick={(e) => suggestedClickHandler(e)}
              >{`${query.value}`}</li>
            ))}
          </ul>
        )}

        {!homeView && queryLog && (
          <ul>
            {queryLog.map((query) => (
              <li  className="queryList">
                <span 
                  id="query_value_span"
                  onClick={(e) => spanClickHandler(e)}
                >{query.value}</span>
                <button type="button" 
                  className="deleteBtn"
                  onClick={(e) => closeHandler(e)}
                >
                  X
                </button>
              </li>
            ))}
          </ul>
        )}
      </form>
    </>
  );
}
