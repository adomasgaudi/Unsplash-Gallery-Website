import React from 'react'
import { useDispatch } from 'react-redux'
import { changeView } from '../imageGallery/gallerySlice'

export default function NavBar() {
  const dispatch = useDispatch()

  
  const homeHandler = () => {
    dispatch(changeView(true))
  }
  const searchHandler = () => {
    dispatch(changeView(false))
  }

  return (
    <div className="navbarBottom">
      <div className="buttons">
        <button onClick={()=>homeHandler()} >home</button>
        <button onClick={()=>searchHandler()} >search</button>
      </div>
    </div>
  )
}
