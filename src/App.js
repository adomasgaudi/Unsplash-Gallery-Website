import React, { useEffect, useState } from 'react';
import './App.scss';
import ImageGallery from './features/imageGallery/ImageGallery';
import Input from './features/input/Input';
import Footer from './features/Components/Footer';
import Navigation from './features/Components/NavBar';

function App() {
  
  return (
  <>
    <div className="spacey"></div>
    <Input/>
    <section>
      <ImageGallery/>
      <Navigation/>
    </section>
    <Footer></Footer>
  </>
  
  );
}

export default App;
